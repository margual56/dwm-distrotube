# My dwm (dynamic window manager) build

This is a fork of [DistroTube's dwm build](https://gitlab.com/dwt1/dwm-distrotube). Please, check out his repository for further instructions on how to install this thing from scratch.
	
# My Keybindings

The MODKEY is set to the Super key (aka the Windows key).  I try to keep the
keybindings consistent with all of my window managers, but if you several of my window manager configs, there may be some discrepancies between them.

## Main keybindings

| Keybinding | Action |
| :--- | :--- |
| `MODKEY + RETURN` | opens the rofi run menu (`rofi -show run`) |
| `MODKEY + SHIFT + RETURN` | opens terminal (alacritty is the terminal but can be easily changed) |
| `MODKEY + q` | closes window with focus |
| `MODKEY + SHIFT + r` | restarts dwm |
| `MODKEY + SHIFT + ALT + q` | quits dwm |
| `MODKEY + b` | hides the bar |
| `MODKEY + 1-9` | switch focus to workspace (1-9) |
| `MODKEY + SHIFT + 1-9` | send focused window to workspace (1-9) |
| `MODKEY + j` | focus stack +1 (switches focus between windows in the stack) |
| `MODKEY + k` | focus stack -1 (switches focus between windows in the stack) |
| `MODKEY + SHIFT + j` | rotate stack +1 (rotates the windows in the stack) |
| `MODKEY + SHIFT + k` | rotate stack -1 (rotates the windows in the stack) |
| `MODKEY + h` | setmfact -0.05 (expands size of window) |
| `MODKEY + l` | setmfact +0.05 (shrinks size of window) |
| `MODKEY + .` | focusmon +1 (switches focus next monitors) |
| `MODKEY + ,` | focusmon -1 (switches focus to prev monitors) |

## Layout controls

| Keybinding | Action |
| :--- | :--- |
| `MODKEY + d` | row layout |
| `MODKEY + i` | column layout |
| `MODKEY + TAB` | cycle layout (-1) |
| `MODKEY + SHIFT + TAB` | cycle layout (+1) |
| `MODKEY + SPACE` | change layout |
| `MODKEY + SHIFT + SPACE` | toggle floating windows |
| `MODKEY + t` | tile layout |
| `MODKEY + f` | floating layout |
| `MODKEY + m` | monocle layout |
| `MODKEY + g` | grid layout |

## Application controls

| Keybinding | Action |
| :--- | :--- |
| `MODKEY + ALT + b` | open Brave browser |
| `MODKEY + ALT + s` | executes `tabbed -r 2 surf -pe x '.surf/html/homepage.html'` |
| `MODKEY + ALT + m` | open [mailspring](https://github.com/Foundry376/Mailspring) |
| `MODKEY + ALT + f` | open [pcmanfm (PaCMANFileManager)](https://wiki.archlinux.org/title/PCManFM) |
| `Print` (print screen button) | opens flameshot in GUI mode |

## Doom emacs

| Keybinding | Action |
| :--- | :--- |
| `CTRL + e + e` | `emacsclient -c -a 'emacs'` |
| `CTRL + e + d` | `emacsclient -c -a 'emacs' --eval '(dired nil)'` |
| `CTRL + e + m` | `emacsclient -c -a 'emacs' --eval '(mu4e)'` |
| `CTRL + e + b` | `emacsclient -c -a 'emacs' --eval '(ibuffer)'` |
| `CTRL + e + n` | `emacsclient -c -a 'emacs' --eval '(elfeed)'` |
| `CTRL + e + s` | `emacsclient -c -a 'emacs' --eval '(eshell)'` |  
| `CTRL + e + v` | `emacsclient -c -a 'emacs' --eval '(+vterm/here nil)'` |

# Running dwm

If you do not use a login manager (such as lightdm) then you can add the following line to your .xinitrc to start dwm using startx:

    exec dwm
	
If you use a login manager (like lightdm), make sure that you have a file called dwm.desktop in your /usr/share/xsessions/ directory.  It should look something like this:

	[Desktop Entry]
	Encoding=UTF-8
	Name=Dwm
	Comment=Dynamic window manager
	Exec=dwm
	Icon=dwm
	Type=XSession


# Configuring dwm-distrotube

If you installed dwm-distrotube-git using the AUR, then the source code can be found in /opt/dwm-distrotube-git.  If you downloaded the source and built dwm-distrotube yourself, then the source in the directory that you downloaded.  The configuration of dwm-distrotube is done by editng the config.h and (re)compiling the source code.  

_Note: it's the same for my build, except I do not have it in the AUR. First install DistroTube's and then, replace the folder with this repository._

	sudo make install
